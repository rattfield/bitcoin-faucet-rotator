<footer class="footer">
    <div class="container">
        <p class="text-center">&copy; <a href="http://www.robertattfield.com" target="_blank" title="Rob Attfield">Rob Attfield</a> 2014 to {{ \Carbon\Carbon::now()->year }}. View the original <a href="http://freebtc.website" target="_blank" title="Bitcoin Faucet Rotator">Bitcoin Faucet Rotator</a></p>
        <p class="text-center"><small><a href="http://critic.net/" target="_blank">Critic.net Free Slots</a></small></p>
    </div>
</footer>
